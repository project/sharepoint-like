<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">

  <head>
    <title><?php print $head_title ?></title>
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
  </head>

<body>
  <div id="wrap">

    <div id="head">
      <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print base_path() ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a><?php } ?>
			<span class="small"><br />&#8226; &#8226; &#8226; &#8226; &#8226;</span></h1> 
      <?php if ($site_slogan) { ?><p id="slogan"><?php print $site_slogan ?></p><?php } ?>	 
    </div>

    <?php if (isset($primary_links)) { ?><div id="primary"><?php print theme('links', $primary_links) ?></div><?php } ?>

    <div id="left">
      <?php if ($left) { ?>
        <?php print $left ?>
      <?php } ?>
      <?php if ($right != ""): ?>
        <?php print $right ?> <!-- print right sidebar if any blocks enabled -->
      <?php endif; ?> 
    </div>

    <div id="main">
      <div id="breadcrumb">
       <?php print $breadcrumb ?>
      </div>

      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <?php print $help ?>
      <?php print $messages ?>

      <h1 class="title"><?php print $title ?></h1>
      <div class="tabs"><?php print $tabs ?></div>

      <?php print $content; ?>

    </div>
    <div id="footer">
      <?php print $footer_message ?>
    </div>
    <?php print $closure ?>

</div>


</body>
</html>

